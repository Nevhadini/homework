import java.util.Scanner;

public class Homework3 {
    public static void main(String[] args) {
        game();
    }

    public static void game() {
        Scanner scan = new Scanner(System.in);
        String s;
        while (true) {
            System.out.print("Please, input the day of the week:");
            s = scan.nextLine();
            String[][] array = arrayForGame();

            if (s.equals("exit")) {
                break;
            }
            switch (s.toLowerCase()) {
                case "sunday ", "sunday" -> System.out.println(array[0][1]);
                case "monday ", "monday" -> System.out.println(array[1][1]);
                case "tuesday ", "tuesday" -> System.out.println(array[2][1]);
                case "wednesday ", "wednesday" -> System.out.println(array[3][1]);
                case "thursday ", "thursday" -> System.out.println(array[4][1]);
                case "friday ", "friday" -> System.out.println(array[5][1]);
                case "saturday ", "saturday" -> System.out.println(array[6][1]);
                default -> System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }



    public static String [][] arrayForGame(){
        String[][] array = new String[7][2];
        array[0][0] = "sunday";array[0][1] = "Your tasks for Sunday:  Do home work";
        array[1][0] = "monday";array[1][1] = "Your tasks for Monday:  Watch a film";
        array[2][0] = "tuesday";array[2][1] = "Your tasks for Tuesday:  Meet friends";
        array[3][0] = "wednesday";array[3][1] = "Your tasks for Wednesday:  Do sports";
        array[4][0] = "thursday";array[4][1] = "Your tasks for Thursday:  learn English";
        array[5][0] = "friday";array[5][1] = "Your tasks for Friday:  Go to a party";
        array[6][0] = "saturday";array[6][1] = "Your tasks for Saturday:  Sleep all day";
        return array;
    }
}
