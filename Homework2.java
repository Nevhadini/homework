import java.util.Scanner;

public class Homework2 {
    static int[][] battlefield = new int[5][5];
    static int[][] monitor = new int[5][5];
    static String playerName;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Write yor name:");
        playerName = scanner.nextLine();
        System.out.println("Hello, " + playerName);
        System.out.println("All set. Get ready to rumble!");
        placeGoal(battlefield);
        takeAshot(playerName, monitor, battlefield);
        showMonitor();
    }
    public static void showMonitor() {
        System.out.println("   0 1 2 3 4 ");
        for (int i = 0; i < monitor.length; i++) {
            System.out.print(i + "  ");
            for (int j = 0; j < monitor.length; j++) {
                if (monitor[i][j] == 0) {
                    System.out.print("- ");
                } else if (monitor[i][j] == 1) {
                    System.out.print("X ");
                } else {
                    System.out.print("* ");
                }
            }
            System.out.println();
        }
    }

    public static void placeGoal(int[][] battlefield) {
        int deck = 3;
        int randomNumberX = (int) (Math.random() * 3);
        int randomNumberY = (int) (Math.random() * 3);
        int randomNumber = (int) (Math.random() * 2);

        do {
            for (int i = 0; i < deck; i++) {
                if (randomNumber == 1) {
                    battlefield[randomNumberX][randomNumberY + i] = 1;
                } else {
                    battlefield[randomNumberX + i][randomNumberY] = 1;
                }
            }
        } while (isLevel(randomNumberX, randomNumberY, deck, randomNumber));
    }

    public static void takeAshot(String playerName, int[][] monitor, int[][] battlefield) {
        while (!isWin()){
            System.out.println(playerName + ", take a shot!");
            System.out.println("   0 1 2 3 4 ");
            for (int i = 0; i < monitor.length; i++) {
                System.out.print(i + "  ");
                for (int j = 0; j < monitor.length; j++) {
                    if (monitor[i][j] == 0) {
                        System.out.print("- ");
                    } else if (monitor[i][j] == 1) {
                        System.out.print("X ");
                    } else {
                        System.out.print("* ");
                    }
                }
                System.out.println();
            }
            System.out.println();
            System.out.println("Enter the x-axis coordinates:");
            if (!scanner.hasNextInt()){
                System.out.println("It is not a number!");
                scanner.next();
                continue;
            }
            int x = scanner.nextInt();
            if (x > 4 || x < 0) {
                System.out.println("Wrong number, it must be 0,1,2,3 or 4!!! ");
                continue;
            }
            System.out.println("Enter the y-axis coordinates:");
            if (!scanner.hasNextInt()){
                System.out.println("It is not a number!");
                scanner.next();
                continue;
            }
            int y = scanner.nextInt();
            if (y > 4 || y < 0) {
                System.out.println("Wrong number, it must be 0,1,2,3 or 4!!! ");
                continue;
            }
            if (battlefield[x][y] == 1) {
                System.out.println("Sniper, you hit!!!");
                monitor[x][y] = 1;
            } else {
                System.out.println("Loser, you didn't get it");
                monitor[x][y] = 2;
            }
        }

    }
    public static boolean isLevel(int randomNumberX,int randomNumberY, int deck,int randomNumber) {
        if (randomNumber == 1) {
            return randomNumberY + deck > 10;
        }
        if (randomNumber == 2) {
            return randomNumberX + deck <= 10;
        }return false;
    }

    public static boolean isWin() {
        int counter = 0;
        for (int[] ints : monitor) {
            for (int anInt : ints) {
                if (anInt == 1) {
                    counter++;
                }
            }
        }
        if (counter >= 3) {
            System.out.println(playerName + ", You have won!");
            return true;
        } else {
            return false;
        }
    }
}