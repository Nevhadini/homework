package Homework4;

public class Main {
    public static void main(String[] args) {
            Human father1=new Human("Vanya","Nevhad",1995,130);
            Human mother1=new Human("Katya","Nevhad",2000,140);
            Pet pet1=new Pet("Dog","Poops",15,50);
            Family family1 =new Family(mother1,father1);
            Human child1=new Human("Jora","Nevhad",2024,110);
            family1.addChildren(child1);
            family1.setPet(pet1);
            pet1.setHabits("Play");
            family1.deleteChild(0);

            System.out.println(family1);
            mother1.describePet();


    }
}
