package Homework4;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human [] children;
    private Pet pet;

    public Family() {
    }
    public Family(Human mother,Human father){
        mother.setFamily(this);
        father.setFamily(this);
        this.mother=mother;
        this.father=father;
        this.children=new Human[]{};
    }

    public void addChildren(Human child){
        child.setFamily(this);
        this.children=Arrays.copyOf(this.children,this.children.length+1);
        this.children[this.children.length-1]=child;
    }
    public boolean deleteChild(int index){
        Human [] array= new Human[this.children.length-1];
        for (int i = 0,j=0; i < this.children.length; i++) {
            if(i!=index){
                array[j]=this.children[i];
                j++;
            }

        }
        if(this.children.length>=index){this.children=array;}
        return this.children.length >= index;
    }
    public int countFamily(){
        return 2 + children.length;
    }


    public void setMother(Human mother){

        this.mother=mother;
    }
    public Human getMother(){

        return mother;
    }

    public void setFather(Human father){

        this.father=father;
    }
    public Human getFather(){

        return father;
    }

    public void setChildren(Human[] children){

        this.children=children;
    }
    public Human [] getChildren(){

        return children;
    }
    public void setPet(Pet pet){

        this.pet=pet;
    }
    public Pet getPet(){
        return pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }
    @Override
    public String toString() {
        return "Family{" +
                "\n mother:" + mother +
                "\n father:" + father +
                "\n children:" + Arrays.toString(children) +
                "\n pet:" + pet +
                '}';
    }
}
