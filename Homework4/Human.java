package Homework4;

import java.util.Objects;

public class Human {
    private Family family;
    private String name;
    private String surname;
    private int year;
    private int iq;

    public Human() {}

    public Human(String name, String surname, int year) {
         this.name=name;
         this.surname=surname;
         this.year=year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name=name;
        this.surname=surname;
        this.year=year;;

    }
    public Human(String name, String surname, int year,Family family,int iq) {
        this.name=name;
        this.surname=surname;
        this.year=year;
        this.family=family;
        this.iq=iq;
    }

    public Family getFamily() {
        return family;
    }
    public void greetPet() {
        System.out.println("Привет," + family.getPet().getNickname());
    }

    public void describePet() {
        String trickLevel = family.getPet().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
        System.out.println("У меня есть " + family.getPet().getSpecies() + ", ему " + family.getPet().getAge() + "  лет, он " + trickLevel);
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setName(String name){this.name=name;}
    public String getName(){return name;}

    public void setSurname(String surname){this.surname=surname;}
    public String getSurname(){return surname;}

    public void setYear(int year){this.year=year;}
    public int getYear(){return year;}

    public void setIq(int iq){this.iq=iq;}
    public int getIq(){return iq;}


    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }


    @Override
    public String toString() {
        return "Human{name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && name.equals(human.name) && surname.equals(human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }
}
