package Homework4;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.println("Привет, хозяин. Я - " + nickname + ". Я соскучился!");
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        if(trickLevel>=0&&trickLevel<=100){this.trickLevel = trickLevel;}
        else {this.trickLevel=-1;}
        this.habits=new String[]{};
    }
    public void setSpecies(String species){
        this.species=species;
    }
    public String getSpecies(){
        return species;
    }

    public void setNickname(String nickname){
        this.nickname=nickname;
    }
    public String getNickname(){
        return nickname;
    }

    public void setAge(int age){
        this.age=age;
    }
    public int getAge(){
        return age;
    }

    public void setTrickLevel(int trickLevel){
        this.trickLevel=trickLevel;
    }
    public int getTrickLevel(){
        return trickLevel;
    }

    public void setHabits(String habits){
        this.habits=new String[this.habits.length+1];
        this.habits[this.habits.length-1]=habits;
    }
    public String[] getHabits(){
        return habits;
    }



    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return species.equals(pet.species) && nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname);
    }
}
